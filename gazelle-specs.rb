#!/usr/bin/env ruby

require "json"
require "yaml"
require "faraday"
require "faraday-cookie_jar"
require "htmlentities"

begin
  web_url, torrent_id = ARGV.first.match(/(https:\/\/[[:alpha:]\.]+)\/torrents\.php.*torrentid=([[:digit:]]+)/)[1..2]
rescue NoMethodError
  abort "Usage: ./gazelle-specs.rb \"https://gazelle.tld/torrents.php?torrentid=<torrent_id>\""
end

begin
  config = YAML::load_file('./specs.yaml')
rescue
  abort "ABORT: Can't read the specs.yaml file"
end

ZOOMS = config["zooms"]
VERBOSE = config["verbose"]
FLAC_DIR = config["flacdir"]
GAZELLE_COOKIE = config["cookies"][web_url[/\/\/([[:alnum:]]+)/, 1]]
IMAGE_SERVICE = config["imageupload"]["service"].downcase
IMGUR_CLIENT_ID = config["imageupload"]["data"]["imgur_client_id"]
PTPIMG_API_KEY = config["imageupload"]["data"]["ptpimg_api_key"]

class GazelleAPI
  APIError = Class.new StandardError
  AuthError = Class.new StandardError
  PostError = Class.new StandardError

  attr_reader :connection

  def initialize(tracker)
    @connection = Faraday.new(url: tracker) do |faraday|
      faraday.use :cookie_jar
      faraday.request :multipart
      faraday.request :url_encoded
      faraday.options.params_encoder = Faraday::FlatParamsEncoder
      faraday.adapter :net_http
    end
  end

  def authenticated?
    @authenticated
  end

  def set_cookie(cookie)
    connection.headers["Cookie"] = cookie
    @authenticated = true
  end

  def fetch(resource, parameters = {})
    unless authenticated?
      raise AuthError
    end

    res = connection.get "/ajax.php", parameters.merge(:action => resource)

    if res.status == 302 && res["location"] == "login.php"
      raise AuthError, connection.host, res.status
    elsif !res.success?
      raise APIError, connection.host, res.status
    end

    parsed_res = JSON.parse res.body

    if parsed_res["status"] == "failure"
      raise APIError, parsed_res["error"]
    end

    parsed_res["response"]
  end

  def set_api_key(key)
    connection.headers["Authorization"] = key
    @authenticated = true
  end

  def post(resource, parameters = {})
    unless authenticated?
      raise AuthError
    end

    res = connection.post "/ajax.php", parameters.merge(:action => resource)

    if res.status == 302 && res["location"] == "login.php"
      raise AuthError
    end

    parsed_res = JSON.parse res.body

    if parsed_res["status"] == "failure" || parsed_res["status"] == 400
      raise PostError.new "#{parsed_res["error"]}"
    end
    parsed_res["response"]
  end

  def update_release(web_url, payload, torrent_id)
    unless authenticated?
      raise WhatCD::AuthError
    end
    path = "#{web_url}/torrents.php?action=edit&id=#{torrent_id}"
    res = connection.post path, payload
    unless res.status == 302 && res.headers["location"] =~ /torrents/
      raise APIError, res.status
    end
  end
end

def ptpimg_upload(file, ptpimg_api_key)
  conn = Faraday.new(url: "https://ptpimg.me") do |f|
    f.request :multipart
    f.request :url_encoded
    f.adapter :net_http
  end
  begin
    response = conn.post do |req|
      req.url '/upload.php'
      req.options.timeout = 2
      req.options.open_timeout = 2
      req.body = {
        "file-upload[0]": Faraday::UploadIO.new(file, 'image/jpeg'),
        "api_key": ptpimg_api_key
      }
    end
  rescue Exception => e
    abort "ABORT: Can't contact ptpimg API: #{e}"
  end
  if response.status == 200
    respbody = JSON.parse(response.body).first
    return "https://ptpimg.me/#{respbody["code"]}.#{respbody["ext"]}"
  else
    abort "ABORT: Could not upload #{file} to PTP, HTTP response status: #{response.status}"
  end
end

def imgur_upload(file, imgur_client_id)
  conn = Faraday.new(url: "https://api.imgur.com/3/upload.json") do |f|
    f.request :multipart
    f.request :url_encoded
    f.adapter :net_http
  end
  begin
    response = conn.post do |req|
      req.headers["Authorization"] = 'Client-ID ' + imgur_client_id
      req.options.timeout = 2
      req.options.open_timeout = 2
      req.body = {
        "image": Faraday::UploadIO.new(file, 'image/jpeg'),
      }
    end
  rescue Exception => e
    puts "ABORT: Can't contact imgur API: #{e}"
  end
  if response.status == 200
    respbody = JSON.parse(response.body)
    return "#{respbody["data"]["link"]}"
  elsif response.status == 429
    respbody = JSON.parse(response.body)
    waittime = respbody["data"]["error"]["message"].delete("^0-9")
    print "API 429, waiting #{waittime} minutes .. "
    sleep waittime.to_i * 60
    imgur_upload(file, imgur_client_id)
  else
    abort "ABORT: Could not upload #{file} to imgur, HTTP response status: #{response.status}#{response.status == 403 ? "; Check if your Client-ID is correct" : ""}"
  end
end

api = GazelleAPI.new(web_url)
if GAZELLE_COOKIE
  api.set_cookie GAZELLE_COOKIE
end
if !api.authenticated?
  abort "ERROR: Please specify a valid cookie for '#{web_url[/\/\/([[:alnum:]]+)/, 1]}' in specs.yaml."
end

authkey = api.fetch(:index)["authkey"]
torrent = api.fetch :torrent, id: torrent_id
details = torrent["torrent"]
fpath = HTMLEntities.new.decode(torrent["torrent"]["filePath"]).gsub(/\u200E+/, "")
srcdir = "#{FLAC_DIR}/#{fpath}"

unless details['encoding'].include? "Lossless"
  abort "ABORT: Not a FLAC."
end

if details['description'].downcase.include? "spectro"
  abort "ABORT: This torrent (#{fpath}) may already have spectrals in its release description."
end

unless File.directory? srcdir
  abort "Directory (#{srcdir}) not found; no point in generating spectrals."
end

puts "Generating spectrals and uploading to #{IMAGE_SERVICE == "imgur" ? "imgur" : "ptpimg"} - #{fpath}:"
spectrograms = ["[hide=Spectrograms]"]
Dir.chdir("#{srcdir}") do
  system("mkdir", "-p", "spectrals") || abort("ABORT: Unable to create spectrals dir.")
  Dir.glob("./**/*.flac").sort.each do |file|
    output = "spectrals/#{File.basename(file, ".flac")}.png"
    title = File.expand_path(file).gsub(FLAC_DIR + "/", "")
    print "#{title} ... "
    if system("sox", file, "-n", "remix", "1", "spectrogram", "-x", "3000", "-y", "513", "-z", "120", "-w", "Kaiser", "-t", title, "-o", output)
      image_link = IMAGE_SERVICE == "imgur" ? imgur_upload(File.expand_path(output), IMGUR_CLIENT_ID) : ptpimg_upload(File.expand_path(output), PTPIMG_API_KEY)
      spectrograms.push("#{title}:")
      spectrograms.push("[img=#{image_link}]")
      if ZOOMS
        if system("sox", file, "-n", "remix", "1", "spectrogram", "-x", "500", "-y", "1025", "-z", "120", "-w", "Kaiser", "-S", "0:25", "-d", "0:03", "-t", "#{title} - Zoomed", "-o", output)
          image_link = IMAGE_SERVICE == "imgur" ? imgur_upload(File.expand_path(output), IMGUR_CLIENT_ID) : ptpimg_upload(File.expand_path(output), PTPIMG_API_KEY)
          spectrograms.push("[img=#{image_link}]")
        end
      end
      puts "done."
    end
  end
  spectrograms.push("[/hide]")
  system("rm", "-r", "spectrals")
end

if VERBOSE
  puts "---\n" + spectrograms.join("\n")
end

payload = {
  auth: authkey,
  action: "takeedit",
  torrentid: torrent_id,
  groupid: torrent['group']['id'],
  release_desc: "#{details['description']}\n#{spectrograms[0..1].join + "\n" + spectrograms[2..-1].join("\n")}",
  remaster_title: details['remasterTitle'],
  remaster_year: details['remasterYear'],
  remaster_record_label: details['remasterRecordLabel'],
  remaster_catalogue_number: details['remasterCatalogueNumber'],
  media: details['media'],
  format: details['format'],
  bitrate: details['encoding'],
  groupremasters: 0,
  type: 1,
  submit: "true"
}

print "---\nAdding spectrograms to release description ... "
api.update_release(web_url, payload, torrent_id)
puts "done."
